/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rectangles;

import org.junit.Assert.*;
/**
 *
 * @author iuabd
 */
public class ShapesDemo {
    
    public static void calculateArea(Rectangle r){
        
        r.setWidth(2);
        r.setHeight(3);
        
        assertEquals( "Area calculation is incorrect", r.getArea(), 6);
    }
    
    public static void main(String[] args) {
        
        ShapesDemo.calculateArea( new Rectangle() );
    }
    
}
