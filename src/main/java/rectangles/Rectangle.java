/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rectangles;

/**
 *
 * @author iuabd
 */
public class Rectangle {
    
    //attribute(s)
    protected double height;
    protected double width;
    
    //constructor(s)
    public Rectangle(){}
    
    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }
    
    //getter(s)
    public double getHeight() {
        return height;
    }
    public double getWidth() {
        return width;
    }
    
    //setter(s)
    public void setHeight(double height) {
        this.height = height;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    
    
    //method(s)
    public double getArea(){
        return height*width;
    }
    
    public double getPerimeter(){
        return 2*(height + width);
    }
}
