/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rectangles;

/**
 *
 * @author iuabd
 */
public class Square extends Rectangle{
    
    //constructor(s)
    public Square(double height, double width) {
        super(height, width);
    }
    
    //method(s)
    @Override
    public double getArea(){
        return height*width;
    }
    
    
    @Override
    public double getPerimeter(){
//        if(height != width){
//            return 0;
//        }
//        else{
            return 2*(height + width);
        
    }
    
    
}
